import React from "react";
import auth from "../services/authService";
import http from "../services/httpService";
import loginimage from "../gbiBank/login.jpg";
export default class Login extends React.Component{
    state={
        detail:{email:"",password:""},
        error:{}
    };
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.handleValidate(e);
        this.setState(s1);
    }
   async postData(url,obj){
    try{
        let response = await http.post(url,obj);
        let{data}=response;
        auth.login(data);
        if(data.role=="admin"){
            window.location="/admin"
        }else if(data.role=="student"){
            window.location="/student"
        }else{
            window.location="/faculty"
        }
    }catch(ex){
        if(ex.response && ex.response.status===500){
            let errors={};
            errors.email=ex.response.data;
            console.log(errors.email);
            this.setState({errors:errors})
        }
    }
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let errors=this.validateForm();
        if(this.isValid(errors)){
        this.postData("/login",this.state.detail);
        }else{
            let s1 = {...this.state};
            s1.error=errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return count==0
    }
    handleValidate=(e)=>{
        let s1 = {...this.state};
        const{currentTarget:input}=e;
        switch(input.name){
            case "password":s1.error.password = this.validatePassword(input.value);break;
            default : break;
        }
        this.setState(s1);
    }
    validateForm=()=>{
        const{password}=this.state.detail;
        let errors={};
        errors.password = this.validatePassword(password);
        return errors;
    }
    validatePassword=(val)=>
    !val?"Please Enter Password":val.length<7?"Password must be 7 characters":""
    render(){
        const{email,password}=this.state.detail;
        const{errors=null,error}=this.state;
        return <div className="jumbotron-fluid bg-info w-100 h-100 text-center">
            <h1 className="display-3 text-danger"><b>Login Page!</b></h1>
            <img className="img-fluid my-3" width="130px" src={loginimage}/>
            <div className="row marginrow">
                <div className="col-4"></div>
                <div className="col-4">
                    <div className="login1 border">
                        {errors && <span className="text-danger font-weight-bold">{errors.email}</span>}
            <div className="form-group">
                <label className="text-center text-success" >Email</label>
                <input type="text" className="form-control" name="email" value={email} 
                onChange={this.handleChange}placeholder="Enter Email"/>
                <small><b>We'll never share your Detail with any one else.!</b></small>
            </div>
            <div className="form-group ">
                <label className="text-center text-success" >Password</label>
                <input type="password" className="form-control" name="password" value={password} 
                onChange={this.handleChange} placeholder="Enter Password"/>
                {error?<span className="text-danger font-weight-bold">{error.password}</span>:""}
            </div>
            <div className="text-center">
            <button className="btn btn-danger my-3" onBlur={this.handleValidate} onClick={this.handleSubmit}>Login</button> 
            </div>
            </div>
            </div>
            </div>
                

        </div>
    }
}