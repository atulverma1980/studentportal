import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
import EditClass from "./editClass";
export default class AllScheduled extends React.Component{
    state={
        classdetail:[],
        data:[],
        view:-1,
    }
    async componentDidMount(){
        let user = auth.getItem();
        let response = await http.get(`/getFacultyClass/${user.name}`);
        let{data}=response;
        this.setState({classdetail:data});
    }
    edit=(arr)=>{
        let s1={...this.state};
        s1.view=1;
        s1.data=arr;
        this.setState(s1);
    }
    async putData(url,obj){
        let response = await http.put(url,obj);
        alert("Detail Updated Successfully");
        this.setState({view:-1});
    }
    handleChange=(opt)=>{
        let s1={...this.state};
        this.putData(`/postClass/${opt.classId}`,opt);
    }
    render(){
        const{classdetail,view,data}=this.state;
        return<div className="container my-4 bg-light">
           {view===-1?<React.Fragment>
           <h2 className="display text-center text-warning"><u>All Scheduled Classes</u></h2>
            <div className="row border add3 text-center">
                <div className="col-2 border"><b>Course Name</b></div>
                <div className="col-2 border"><b>Start Time</b></div>
                <div className="col-2 border"><b>End Time</b></div>
                <div className="col-3 border"><b>Topic</b></div>
                <div className="col-3 border"></div>
            </div>
            {classdetail.map(v=><div className="row text-center table4" key={v.classId}>
                <div className="col-2 border">{v.course}</div>
                <div className="col-2 border">{v.time}</div>
                <div className="col-2 border">{v.endTime}</div>
                <div className="col-3 border">{v.topic}</div>
                <div className="col-3 border">
                    <button className="btn btn-primary"onClick={()=>this.edit(v)}>Edit</button>
                </div>
            </div>)}
            <div className="text-center">
                <button className="btn btn-success my-2">Add New Class</button>
            </div>
            </React.Fragment>:<EditClass detail={data} onOption={this.handleChange}/>}
        </div>
    }
}