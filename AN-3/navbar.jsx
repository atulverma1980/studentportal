import React from "react";
//import auth from "../services/authService";
import {Link} from "react-router-dom";
export default class Navbar extends React.Component{
    render(){
        const {user}=this.props;
        return <nav className="navbar navbar-expand-md navbar-success bg-success">
             <Link
              to={user ? user.role=="admin"?"/admin":user.role=="student"?"/student":user.role=="faculty"?"/faculty":"/":"/login"} className="navbar-brand text-dark font-weight-bold">
      {user ? user.role=="admin"?"Home":user.role=="student"?"Student Home":user.role=="faculty"?"Faculty Home":"Home":"Home"} 
        </Link>
        <div className="">
            <ul className="navbar-nav mr-auto">
            {user && user.role=="admin" && (
                <li className="nav-item text-dark">
                    <Link className="nav-link text-dark" to="/register"><h5>Register</h5></Link>
                </li>)}
             {user && user.role==="admin" &&(
            <li className="nav-item dropdown">
        <h5 className="nav-link dropdown-toggle text-dark"  id="navbarDropdown"
         role="button" data-toggle="dropdown" aria-haspopup="true">
          Assign
        </h5>
        <div className="dropdown-menu text-dark" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item text-dark" to="/studentCourse"><b>Student to Course</b></Link>
          <Link className="dropdown-item text-dark" to="/facultyCourse"><b>Faculty to Course</b></Link>
        </div>
      </li>)}
      {user && user.role==="admin" && (
      <li className="nav-item dropdown">
        <h5 className="nav-link dropdown-toggle text-dark" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          View
        </h5>
        <div className="dropdown-menu text-dark" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item text-dark" to="/allStudents?page=1"><b>All Student</b></Link>
          <Link className="dropdown-item text-dark" to="/allFaculties?page=1"><b>All Faculty</b></Link>
        </div>
      </li>)}
      {user && user.role==="student" && (
      <li className="nav-item dropdown text-dark">
       <Link className="nav-link text-dark" to="/studentDetail"><h5>Student Detail</h5></Link>
      </li>
      )}
        {user && user.role==="student" && (
      <li className="nav-item dropdown text-dark">
       <Link className="nav-link text-dark" to="/allClasses"><h5>All Classes</h5></Link>
      </li>
      )}
        {user && user.role==="student" && (
      <li className="nav-item dropdown text-dark">
       <Link className="nav-link text-dark" to="/courseStudent"><h5>All Courses</h5></Link>
      </li>
      )}
      {user && user.role==="faculty" && (
      <li className="nav-item dropdown text-dark">
       <Link className="nav-link text-dark" to="/courseAssigned"><h5>Courses</h5></Link>
      </li>)}
      {user && user.role==="faculty" && (
      <li className="nav-item dropdown text-dark">
        <h5 className="nav-link dropdown-toggle text-dark"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Class Details
        </h5>
        <div className="dropdown-menu text-dark" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item text-dark" to="/scheduleClass"><b>Schedule a Class</b></Link>
          <Link className="dropdown-item text-dark" to="/scheduledClasses"><b>All Scheduled Classes</b></Link>
        </div>
      </li>)}
            </ul>
        </div>
        <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul className="navbar-nav ml-auto">
                {user && (
            <li className="nav-item">
                    <h5 className="nav-link text-dark font-weight-bold">Welcome {user.name}  <b>||</b></h5>
                </li>)}
                {!user && (
                <li className="nav-item text-dark">
                    <Link className="nav-link text-dark font-weight-bold" to="/login">Login</Link>
                </li>)}
                {user && (
                <li className="nav-item text-dark">
                    <Link className="nav-link text-dark font-weight-bold" to="/logout">Logout</Link>
                </li>)}
            </ul>
        </div>
        </nav>
    }
}