import React from "react";
import image from "./error.jpg";
export default class NotAllowed extends React.Component{
    render(){
        return <div className="container text-center">
            <h2 className="display-2 text-center text-danger my-4"><b>This Functionality is Not Allowed!!</b></h2>
            <img className="img-fluid my-4" src={image} width="20%"/>
        </div>
    }
}