import React from "react";
import image from "./faculty.jpg";
export default class Faculty extends React.Component{
    render(){
       return <React.Fragment>
       <div className="container-fluid bg-light">
            <h2 className="display-2 text-danger text-center my-4"><b>Welcome To Faculty Dashbord.</b></h2>
        </div>
        <div className="jumbotron-fluid text-center my-4 bg-light">
            <img src={image} className="img-fluid" width="50%"/>
        </div>
        </React.Fragment>
    }
}