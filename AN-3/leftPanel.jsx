import React from "react";
import http from "../services/httpService.js"
export default class LeftPanel extends React.Component{
    state={
        courses:[],
    };
    async componentDidMount(){
        let response = await http.get("/getCourses");
        let{data}=response;
        this.setState({courses:data});
    }
    makeCB=(arr,value,name,label)=>(
        <React.Fragment>
            <label className="font-weight-bold lb text-center">{label}</label>
            {arr.map(v=><div className="form-check"key={v.name}>
                <input type="checkbox" className="form-check-input" name={name} value={v.name}
                checked={value.findIndex(a=>a===v.name)>=0} onChange={this.handleChange}/>
                <label className="form-check-label cb">{v.name}</label>
            </div>)}
        </React.Fragment>
    );
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let opt = {...this.props.option};
        opt[input.name]=this.updateCB(opt[input.name],input.checked,input.value);
        this.props.onOptionChange(opt);
    }
    updateCB=(inpVal,checked,value)=>{
        let inpArr = inpVal?inpVal.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex(v=>v===value);
            if(index>=0){
                inpArr.splice(index,1);
            }
        }
        return inpArr.join(",");
    }
    render(){
        const{courses=[]}=this.state;
        const{course=""}=this.props.option;
       
       return  <div className="row border">
           <div className="col-12 border">
               {this.makeCB(courses,course.split(","),"course","Options")}
           </div>
       </div> 
    
    }
}