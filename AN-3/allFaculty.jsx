import React from "react";
import http from "../services/httpService.js"
import queryString from "query-string";
import LeftPanel from "./leftPanel";
export default class AllFaculty extends React.Component{
    state={detail:{},number:""};
    async fetchData(){
        const queryParam  = queryString.parse(this.props.location.search);
        let  searchStr=this.makeSearchString(queryParam);
        let response = await http.get(`/getFaculties?${searchStr}`);
        let {data}=response;
        console.log(response);
        let s1 = {...this.state};
       s1.detail = data;
       if(data.page==1){
           s1.number="";
       }
       this.setState(s1);
    }
    componentDidMount(){
        this.fetchData();
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
            this.fetchData();
        }
    }
    handlePage=(incr,val)=>{
        const queryParam = queryString.parse(this.props.location.search);
        let {page="1"}=queryParam;
        let newPage = +page+incr;
        let s1 = {...this.state};
        s1.number = +s1.number+val;
        queryParam.page=newPage;
        this.callURL("/allFaculties",queryParam);
        this.setState(s1);
    }
    callURL=(url,option)=>{
        let searchStr = this.makeSearchString(option);
        this.props.history.push({
            pathname:url,
            search:searchStr,
        });
    }
    handleChange=(option)=>{
        option.page="1";
        this.callURL("/allFaculties",option);
    }
    makeSearchString=(option)=>{
        const{page,course}=option;
        let searchStr ="";
        searchStr = this.addToQuery(searchStr,"page",page);
        searchStr = this.addToQuery(searchStr,"course",course);
        return searchStr;
    }
    addToQuery=(str,name,val)=>
    val?str?`${str}&${name}=${val}`:`${name}=${val}`:str;

    render(){
        const{items=[],page,totalItems,totalNum}=this.state.detail;
        const{number}=this.state;
        const queryParam = queryString.parse(this.props.location.search);
        return <div className="container my-4 bg-light">
         <h2 className="display text-center text-success">All Faculty</h2>
            <div className="row">
            <div className="col-3">
            <LeftPanel option={queryParam} onOptionChange={this.handleChange}/>
            </div>
            <div className="col-9">
                {page==1?page:number} - {page==1?totalItems:totalItems+number} of {totalNum}
            <div className="row border font3">
                <div className="col-4 border text-center"><b>ID</b></div>
                <div className="col-4 border text-center"><b>Name</b></div>
                <div className="col-4 border text-center"><b>Course</b></div>
            </div>
            {items.map((v,index)=><div className="row list3" key={index}>
                <div className="col-4 border text-center">{v.id}</div>
                <div className="col-4 border text-center">{v.name}</div>
                <div className="col-4 border text-center">{v.courses.map(a=>a+" ")}</div>
            </div>)}
            <div className="row">
                <div className="col-2">
                {page>1?<button className="btn btn-success" onClick={()=>this.handlePage(-1,-totalItems)}>Previous</button>:""}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                {totalItems+number==totalNum?"":<button className="btn btn-success"onClick={()=>this.handlePage(1,totalItems)}>Next</button>}
                </div>
            </div>

            </div>
            </div>
        </div>
    }
}