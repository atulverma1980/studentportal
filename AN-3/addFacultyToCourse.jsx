import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class AddFaculty extends React.Component{
    state={
        detail:this.props.detail,
        fac:[],
    }
    async componentDidMount(){
        let response = await http.get("/getFacultyNames");
        let{data}=response;
        this.setState({fac:data});
    }

    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.detail[input.name]=this.updateCB(s1.detail[input.name],input.checked,input.value);
        this.setState(s1);
    }
    updateCB=(inpVal,checked,value)=>{
        let inpArr = inpVal?inpVal:[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex(v=>v===value);
            if(index>=0){
                inpArr.splice(index,1);
            }
        }
        return inpArr;
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.props.onOption(this.state.detail);
    }
    render(){
        const{name,code,description,faculty}=this.state.detail;
        const{fac}=this.state;
        
        return <div className="container">
            <div className="row">
                <div className="col-3"></div>
                <div className="col-6 faculty border">
                <h2 className="display text-center text-danger"><u> Edit the Course</u></h2>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Name:-</label>
                        <div className="col-10">
                          <input type="text" name="name" value={name} className="form-control bg-light my-2" onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Course Code:-</label>
                        <div className="col-10">
                           <input type="text" className="form-control bg-light my-2" name="code" value={code} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Description:-</label>
                        <div className="col-10">
                           <input type="text" className="form-control bg-light my-2" name="description" value={description} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Faculty:-</label>
                    <div className="col-10">
                        {this.makeCB(fac,faculty,"faculty")}
                    </div>
                    </div>
                    <div className="text-center">
                        <button className="btn btn-primary" onClick={this.handleSubmit}>Update</button>
                    </div>
                </div>
            </div>
        </div>
    }
    makeCB=(arr,value,name)=>(
        <React.Fragment>
            {arr.map(v=><div className="form-check"key={v}>
                <input type="checkbox" className="form-check-input" name={name} value={v}
                checked={value.findIndex(a=>a===v)>=0} onChange={this.handleChange}/>
                <label className="form-check-label cb">{v}</label>
            </div>)}
        </React.Fragment>
    );
}