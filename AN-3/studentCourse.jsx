import React from "react";
import http from "../services/httpService";
import AddStudent from "./addStudentToCourse";
export default class StudentCourse extends React.Component{
    state={
        courses:[],
        view:-1,
        data:[],
    };
    async componentDidMount(){
        let response =await http.get("/getCourses");
        let {data}=response;
        this.setState({courses:data});
    }
    edit=(arr)=>{
        let s1={...this.state};
        s1.view=1;
        s1.data=arr;
        this.setState(s1);
    }
    async putData(url,obj){
        let response = await http.put(url,obj);
        this.setState({view:-1});
    }
    handleSubmit=(arr)=>{
        this.putData("/putCourse",arr);
    }
    render(){
        const{courses,data}=this.state;
        return <div className="container bg-light">
            {this.state.view===-1?<React.Fragment>
              <h2 className="display text-center text-warning">Add Students to a Course</h2>
              <div className="row border add">
                  <div className="col-2 text-center border"><b>Course Id</b></div>
                  <div className="col-2 text-center border"><b>Name</b></div>
                  <div className="col-2 text-center border"><b>Course Code</b></div>
                  <div className="col-3 text-center border"><b>Description</b></div>
                  <div className="col-2 text-center border"><b>Students</b></div>
                  <div className="col-1 text-center border"> 
                    </div>                  
              </div>
              {courses.map(v=><div className="row m-2 table1" key={v.name}>
              <div className="col-2 text-center border">{v.courseId}</div>
                  <div className="col-2 text-center border">{v.name}</div>
                  <div className="col-2 text-center border">{v.code}</div>
                  <div className="col-3 text-center border">{v.description}</div>
                  <div className="col-2 text-center border">{v.students.map(a=>a+",")}</div>
                  <div className="col-1 text-center border">
                    <button className="btn btn-primary"onClick={()=>this.edit(v)}>Edit</button>  
                    </div>         
              </div>)}</React.Fragment>:<AddStudent detail={data} onOption={this.handleSubmit}/>}
        </div>
    }
}