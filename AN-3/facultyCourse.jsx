import React from "react";
import http from "../services/httpService";
import AddFaculty from "./addFacultyToCourse";
export default class FacultyCourse extends React.Component{
    state={
        courses:[],
        view:-1,
        data:[],
    };
    async componentDidMount(){
        let response =await http.get("/getCourses");
        let {data}=response;
        this.setState({courses:data});
    }
    edit=(arr)=>{
        let s1={...this.state};
        s1.view=1;
        s1.data=arr;
        this.setState(s1);
    }
    async putData(url,obj){
        let response = await http.put(url,obj);
        this.setState({view:-1});
    }
    handleSubmit=(arr)=>{
        this.putData("/putCourse",arr);
    }
    render(){
        const{courses,data}=this.state;
        return <div className="container bg-light">
            {this.state.view===-1?<React.Fragment>
              <h2 className="display text-center text-warning">Add Faculty to a Course</h2>
              {}
              <div className="row border add1">
                  <div className="col-2 text-center border"><b>Course Id</b></div>
                  <div className="col-2 text-center border"><b>Name</b></div>
                  <div className="col-2 text-center border"><b>Course Code</b></div>
                  <div className="col-3 text-center border"><b>Description</b></div>
                  <div className="col-2 text-center border"><b>Faculty</b></div>
                  <div className="col-1 text-center border"> 
                    </div>                  
              </div>
              {courses.map(v=><div className="row m-2 table2" key={v.name}>
              <div className="col-2 text-center border">{v.courseId}</div>
                  <div className="col-2 text-center border">{v.name}</div>
                  <div className="col-2 text-center border">{v.code}</div>
                  <div className="col-3 text-center border">{v.description}</div>
                  <div className="col-2 text-center border">{v.faculty.map(a=>a+",")}</div>
                  <div className="col-1 text-center border">
                    <button className="btn btn-primary"onClick={()=>this.edit(v)}>Edit</button>  
                    </div>         

              </div>)}
              </React.Fragment>:<AddFaculty detail={data} onOption={this.handleSubmit}/>}
        </div>
    }
}