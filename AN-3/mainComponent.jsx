import React from "react";
import {Route,Redirect,Switch} from "react-router-dom";
import auth from "../services/authService";
import Admin from "./Admin";
import AllClasses from "./allClasses";
import AllFaculty from "./allFaculty";
import AllScheduled from "./allSchedule";
import AllStudent from "./allStudent";
import CourseAssign from "./courseAssign";
import CourseFaculty from "./courseFaculty";
import Faculty from "./faculty";
import FacultyCourse from "./facultyCourse";
import Login from "./login";
import Logout from "./logout";
import Navbar from "./navbar";
import NotAllowed from "./notAllowed";
import Register from "./registerNew";
import ScheduleClass from "./scheduleClass";
import Student from "./student";
import StudentCourse from "./studentCourse";
import StudentDetail from "./studentDetail";
export default class MainComponent extends React.Component{

    render(){
        const user = auth.getItem();
        return <div className="container-fluid">
            <Navbar user={user}/>
            <Switch>
            <Route path="/register"
            render={(props)=>user?user.role=="admin"?<Register {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/scheduleClass"
            render={(props)=>user?user.role=="faculty"?<ScheduleClass {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/courseAssigned"
            render={(props)=>user?user.role=="faculty"?<CourseFaculty {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/scheduledClasses" 
             render={(props)=>user?user.role=="faculty"?<AllScheduled {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/allClasses"
             render={(props)=>user?user.role=="student"?<AllClasses {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/notallowed" component={NotAllowed}/>
            <Route path="/studentDetail"
            render={(props)=>user?user.role=="student"?<StudentDetail {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/studentCourse"
            render={(props)=>user?user.role=="admin"?<StudentCourse {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/courseStudent"
             render={(props)=>user?user.role=="student"?<CourseAssign {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/> 
            <Route path="/facultyCourse"
             render={(props)=>user?user.role=="admin"?<FacultyCourse {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/allStudents"
             render={(props)=>user?user.role=="admin"?<AllStudent {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/admin" component={Admin}/>
            <Route path="/faculty" component={Faculty}/>
            <Route path="/student" component={Student}/>
            <Route path="/allFaculties"
             render={(props)=>user?user.role=="admin"?<AllFaculty {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
            <Route path="/login" component={Login}/>
            <Route path="/logout" component={Logout}/>
            <Redirect from="" to="/"/>
            </Switch>
        </div>

    }
}