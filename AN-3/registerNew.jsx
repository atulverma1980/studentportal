import React from "react";
import http from "../services/httpService";
export default class Register extends React.Component{
    state={
        detail:{name:"",password:"",rePass:"",email:"",role:""},
        error:{}
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.handleValidate(e);
        this.setState(s1);
    }
  async postData(url,obj){
        let response = await http.post(url,obj);
        alert("User Created Successfully!")
        this.props.history.push("/admin");
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s1 = {...this.state};
        let errors=this.validateForm();
        if(this.isValid(errors)){
            let data={name:s1.detail.name,password:s1.detail.password,email:s1.detail.email,role:s1.detail.role};
            this.postData("/register",data);
        }else{
            s1.error = errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        console.log(keys,count);
        return count==0;
    }
    handleValidate=(e)=>{
        let s1 = {...this.state};
        const{currentTarget:input}=e;
        switch(input.name){
            case "password":s1.error.password = this.validatePassword(input.value);break;
            case "rePass":s1.error.rePass = this.validateRepass(input.value);break;
            case "name":s1.error.name=this.validateName(input.value);break;
            case "email":s1.error.email =this.validateEmail(input.value);break;
            default: break;
        }
        this.setState(s1);
    }
    validateForm=(val)=>{
        const{password,rePass,name,email,role}=this.state.detail;
        let errors={};
        errors.password = this.validatePassword(password);
        errors.rePass = this.validateRepass(rePass,password);
        errors.name=this.validateName(name);
        errors.email = this.validateEmail(email);
        errors.role=this.validateRole(role);
        return errors;
    }
    validatePassword=(val)=>
    !val?"Please Enter Password":val.length<7?"minimum length should be 7 Char":""
   validateRepass=(str1,str2)=>
   str1!==str2? "Password does't match":"";
   validateName=(val)=>
   !val?"Please Enter Name":""
   validateEmail=(val)=>
   !val?"Please Enter Email":!val.includes('@')?"Email is not Valid":"";
    validateRole=(val)=>
    !val?"Please Select Role":"";

    render(){
        const{name,password,rePass,email,role}=this.state.detail;
        const{error}=this.state;
        return <div className="container">
            <h2 className="display text-center text-danger">Register</h2>
            <div className="form1">
                <div className="row">
                    <div className="col-3"></div>
                    <div className="col-6 border2">
                <div className="form-group row">
                    <label className="col-sm-3 col-form-label my-3"><h4>Name<span className="text-danger">*</span>:-</h4></label>
                    <div className="col-9">
                    <input type="text" name="name" value={name} className="form-control text-primary bg-light my-4" placeholder="Enter Name" onChange={this.handleChange}/>
                    {error.name?<span className="text-white"><b>{error.name}</b></span>:""}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-3 col-form-label"><h4>Password<span className="text-danger">*</span>:-</h4></label>
                    <div className="col-9">
                    <input type="text" name="password" value={password} className="form-control bg-light" 
                    placeholder="Enter Password" onBlur={this.handleValidate} onChange={this.handleChange}/>
                    {error.password?<span className="text-white"><b>{error.password}</b></span>:""}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-3 col-form-label"><h4>Confirm Password<span className="text-danger">*</span>:-</h4></label>
                    <div className="col-9">
                    <input type="text" name="rePass" value={rePass} className="form-control  bg-light my-3"
                     placeholder="Re-Enter Password" onBlur={this.handleValidate} onChange={this.handleChange}/>
                     {error.rePass?<span className="text-white"><b>{error.rePass}</b></span>:""}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-3 col-form-label"><h4>Email<span className="text-danger">*</span>:-</h4></label>
                    <div className="col-9">
                    <input type="text" name="email" value={email} className="form-control  bg-light my-3"
                     placeholder="Enter Email" onBlur={this.handleValidate} onChange={this.handleChange}/>
                     {error.email?<span className="text-white"><b>{error.email}</b></span>:""}
                    </div>
                </div>
                <div className="form-group row">
                <label className="col-sm-3 col-form-label"><h4>Role<span className="text-danger">*</span>:-</h4></label>
                <div  className="col-9">
                <div className="row">
                    <div className="col-4">
                        <div className="form-check">
                            <input type="radio" name="role" value="student" checked={role=="student"} onChange={this.handleChange} className="form-check-inline"/>
                            <label className="form-check-inline mx-2 font-weight-bold">Student</label>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="form-check">
                            <input type="radio" name="role" value="faculty" checked={role=="faculty"} onChange={this.handleChange} className="form-check-inline"/>
                            <label className="form-check-inline mx-2 font-weight-bold">Faculty</label>
                        </div>
                    </div>
                    </div>  
                    {error.role?<span className="text-white"><b>{error.role}</b></span>:""}    
                </div> 
                </div>
                <div className="text-center">
                <button className="btn btn-success text-center" onClick={this.handleSubmit}>Register</button>
                </div>
            </div>

            </div>
            </div>
        </div>
    }
}