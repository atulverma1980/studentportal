import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class CourseFaculty extends React.Component{
    state={
        coursedetail:[],
    }
    async componentDidMount(){
        let user = auth.getItem();
        let response = await http.get(`/getFacultyCourse/${user.name}`);
        let{data}=response;
        this.setState({coursedetail:data});
    }
    render(){
        const{coursedetail}=this.state;
        return<div className="container my-4 bg-light">
            <h2 className="display text-center text-warning"><u>Course Assigned</u></h2>
            <div className="row border add2 text-center">
                <div className="col-3 border"><b>Course Id</b></div>
                <div className="col-3 border"><b>Course Name</b></div>
                <div className="col-3 border"><b>Course Code</b></div>
                <div className="col-3 border"><b>Description</b></div>
            </div>
            {coursedetail.map(v=><div className="row text-center table2" key={v.courseId}>
                <div className="col-3 border">{v.courseId}</div>
                <div className="col-3 border">{v.name}</div>
                <div className="col-3 border">{v.code}</div>
                <div className="col-3 border">{v.description}</div>
            </div>)}
        </div>
    }
}