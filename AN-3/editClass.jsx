import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class EditClass extends React.Component{
    state={
        detail:this.props.detail,
        courses:[]
    };
    async componentDidMount(){
        let user = auth.getItem();
        let response = await http.get(`/getFacultyClass/${user.name}`);
        let{data}=response;
        this.setState({courses:data});
    };
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1={...this.state};
        s1.detail[input.name]=input.value;
        s1.detail.facultyName=auth.getItem().name;
        this.setState(s1);
    }
   
    handleSubmit=(e)=>{
        e.preventDefault();
        this.props.onOption(this.state.detail);
    }
    render(){
        const{course,time,endTime,topic}=this.state.detail;
        const{courses=[]}=this.state
        console.log(courses);
        return <div className="container">
            <div className="row">
                <div className="col-3"></div>
                <div className="col-6 schedule border">
                <h2 className="display text-center text-danger"><u> Edit a Class</u></h2>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Select Course:-</label>
                        <div className="col-10">
                            <select className="form-control my-3 bg-light text-primary" name="course" value={course} onChange={this.handleChange}>
                                <option value="">Select Course</option>
                                {courses.map((v,index)=><option key={index}>{v.course}</option>)}
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Start Time:-</label>
                        <div className="col-10">
                           <input type="time" className="form-control my-3 bg-light text-primary" name="time" value={time} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">End Time:-</label>
                        <div className="col-10">
                           <input type="time" className="form-control my-3 bg-light text-primary" name="endTime" value={endTime} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Topic:-</label>
                        <div className="col-10">
                           <input type="text" className="form-control my-3 bg-light text-primary" name="topic" value={topic} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="text-center">
                        <button className="btn btn-primary" onClick={this.handleSubmit}>Schedule</button>
                    </div>
                </div>
            </div>
        </div>
    }
}