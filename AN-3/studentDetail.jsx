import React from "react";
import auth from "../services/authService";
import http from "../services/httpService";
export default class StudentDetail extends React.Component{
    state={
        detail:{id:"",name:"",date:"",month:"",year:"",gender:"",about:""},
        showBtn:false,
    };
    async componentDidMount(){
        let user=auth.getItem();
        let response = await http.get(`/getStudentDetails/${user.name}`);
        let{data}=response;
        if(data.about==""){
          let  detail1={id:data.id,name:data.name,date:"",month:"",year:"",gender:"",about:""};
            this.setState({detail:detail1,showBtn:true});
        }else{
            let s1 = {...this.state};
            let index1 = data.dob.indexOf('-');
                    s1.detail.date = data.dob.substring(0,index1);
            let index2 = data.dob.lastIndexOf('-');
                    s1.detail.month = data.dob.substring(index1+1,index2);
                    s1.detail.year = data.dob.substring(index2+1); 
                    s1.detail.about=data.about;
                    s1.detail.gender=data.gender;
                    s1.detail.name=user.name;
                    s1.detail.id=data.id;
                    s1.showBtn=false;
                    this.setState(s1); 
        }
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1={...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
    }
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(obj);
        alert("Detail has been Updated");
        this.props.history.push("/");
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s1 = {...this.state}
            let data={};
            let dob  = s1.detail.date+"-"+s1.detail.month+"-"+s1.detail.year;
            data={...data,id:s1.detail.id,name:s1.detail.name,gender:s1.detail.gender,about:s1.detail.about,dob:dob}
            this.postData("/postStudentDetails",data);
    }
    makeYear=()=>{
        let arr=[];
        for(let i = 2020; i>=1980;i--){
            arr.push(i);
        }
        return arr;
    }
    makeDate=(val)=>{
        let arr=[];
        for(let i=1;i<=val;i++){
            arr.push(i);
        }
        return arr;
    }
    render(){
        const{gender,date,month,year,about,}=this.state.detail
        const{showBtn}=this.state;
        console.log(showBtn);
     let years = this.makeYear();
    const dates =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
     const arr=(month=='April' || month=="June" || month=="September" || month=="November")?this.makeDate(30):month=="Febuary"?this.makeDate(28):dates;
     let  months=["January","Febuary","March","April","May","June","July","August","September","October","November","December"];

        return <div className="container">
              <div className="row">
            <div className="col-3"></div>
                <div className="col-6">
                <h2 className="text-center text-danger">Student Details</h2>
                   <div className=" border customer">
                    <div className="form-group row">
                    <label className="col-sm-2 col-form-label ">Gender<span className="text-danger">*</span>:-</label>
                    <div className="col-10">
                        <div className="form-check">
                            <input type="radio" className="form-check-inline mx-4 my-3" name="gender" value="male"checked={gender=="male"} onChange={this.handleChange}/>
                            <label className="form-check-label">Male</label>
                            <input type="radio" className="form-check-inline mx-4 my-3" name="gender" value="female" checked={gender=="female"}  onChange={this.handleChange}/>
                            <label className="form-check-label">Female</label>
                            {/* {error.gender?<span className="text-danger">{error.gender}</span>:""} */}
                        </div>
                    </div>
                     </div>
                    <hr/>
                    <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Date of Birth<span className="text-danger">*</span>:-</label>
                    <div className="col-10">
                        <div className="row">
                            <div className="col-4">
                            <div className="form-group dd">
                                <select className="form-control bg-light text-primary" onBlur={this.handleValidte} name="year" value={year} onChange={this.handleChange}>
                               <option value="">Select Year</option>
                                {years.map(v=><option key={v}>{v}</option>)}
                                </select>
                                {/* {error.year?<span className="text-danger">{error.year}</span>:""} */}
                            </div>
                            </div>
                            <div className="col-4">
                            <div className="form-group dd">
                                <select className="form-control bg-light text-primary " onBlur={this.handleValidte} name="month" value={month} onChange={this.handleChange}>
                               <option value="">Select Month</option>
                                {year?months.map(v=><option  key={v}>{v}</option>):""}
                                </select>
                                {/* {error.month?<span className="text-danger">{error.month}</span>:""}  */}
                            </div>
                             </div>
                            <div className="col-4">
                            <div className="form-group dd">
                                <select className="form-control bg-light text-primary " onBlur={this.handleValidte} name="date" value={date} onChange={this.handleChange}>
                               <option value="">Select Date</option>
                                {month?arr.map(v=><option  key={v}>{v}</option>):""}
                                </select>
                                {/* {error.date?<span className="text-danger">{error.date}</span>:""} */}
                            </div>
                            </div>
                        </div>
                     </div>
                    </div>
                    <div className="form-group row">
                    <label className="col-sm-2 col-form-label ">About<span className="text-danger">*</span>:-</label>
                    <div className="col-10">
                      <textarea name="about" className="form-control text-primary" value={about} onChange={this.handleChange}/>  
                    </div>
                    </div>
                    <div className="text-center">
                        {showBtn==true?
                        <button className="btn btn-primary" onClick={this.handleSubmit}>Add Detail</button>:""}
                    </div>
                </div>
            </div>
            </div>
        </div>
    }
}