import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class AllClasses extends React.Component{
    state={
        classdetail:[],
    }
    async componentDidMount(){
        let user = auth.getItem();
        let response = await http.get(`/getStudentClass/${user.name}`);
        let{data}=response;
        console.log(user.name);
        console.log(data);
        this.setState({classdetail:data});
    }
    render(){
        const{classdetail}=this.state;
        return<div className="container my-4 bg-light">
            <h2 className="display text-center text-warning"><u>All Scheduled Classes</u></h2>
            <div className="row border allclass text-center">
                <div className="col-2 border"><b>Course Name</b></div>
                <div className="col-2 border"><b>Start Time</b></div>
                <div className="col-2 border"><b>End Time</b></div>
                <div className="col-3 border"><b>Faculty Name</b></div>
                <div className="col-3 border"><b>Topic</b></div>
            </div>
            {classdetail.map(v=><div className="row table4 text-center m-2" key={v.classId}>
                <div className="col-2 border">{v.course}</div>
                <div className="col-2 border">{v.time}</div>
                <div className="col-2 border">{v.endTime}</div>
                <div className="col-3 border">{v.facultyName}</div>
                <div className="col-3 border">{v.topic}</div>
            </div>)}
        </div>
    }
}